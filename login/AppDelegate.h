//
//  AppDelegate.h
//  login
//
//  Created by Click Labs130 on 11/17/15.
//  Copyright (c) 2015 Niteesh Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

